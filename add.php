<?php
include 'class/db.php';
include 'config/db_config.php';
include 'controller/ProductController.php';
//connect database
$db = new db(DB_HOST, DB_USER, DB_PASS, DB_NAME);
// get product types
$product = new ProductController();
$produc_types = $product->getProductsType();
?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Products | Add</title>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1 class="display-4">Product Add</h1>
            </div>
            <form action="add-product.php" method="post">
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" class="form-control" id="sku" name="sku" placeholder="Enter SKU">
                    <small id="skuexist" class="form-text text-danger"></small>
                </div>

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
                </div>

                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control price" id="price" name="price" placeholder="Enter Price" />
                </div>

                <div class="form-group">
                    <label for="name">Type</label>
                    <select name="type" id="type" name="type" class="form-control">
                        <option selected="selected" disabled="disabled">Select Product Type</option>
                        <?php
                        foreach ($produc_types as $produc_type) {
                            ?>
                            <option value="<?= $produc_type['id']; ?>"><?= $produc_type['name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div id="field-append">

                </div>

                <div class="form-group">
                    <button type="submit" name="add-product" id="add-product" class="btn btn-primary float-right">Save</button>
                </div>
            </form>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script>
            (function ($) {
                $.fn.priceInput = function () {
                    this.each(function () {
                        $(this).change(function () {
                            var min = parseFloat($(this).attr("min"));
                            var max = parseFloat($(this).attr("max"));
                            var value = this.valueAsNumber;
                            if (value < min)
                                value = min;
                            else if (value > max)
                                value = max;
                            $(this).val(value.toFixed(2));
                        });
                    });
                };
            })(jQuery);
            $(document).ready(function () {
                $('#price').priceInput();
            });

            //send product type through ajax
            $('#type').change(function () {
                id = $(this).val();
                data = {
                    product_type_id: id
                };

                $.ajax({
                    url: 'get-products-fields.php',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (res) {
                        loadProductFields(res);
                    }
                });
            });

            //send product type through ajax
            $('#sku').blur(function () {
                sku = $(this).val();
                data = {
                    sku: sku
                };

                $.ajax({
                    url: 'check-sku.php',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function (res) {
                        if (res.is_exist) {
                            $('#skuexist').text('SKU Already exist.');
                            $('#add-product').attr('disabled', true);
                        } else {
                            $('#skuexist').text('');
                            $('#add-product').attr('disabled', false);
                        }
                    }
                });
            });


            function loadProductFields(res) {
                console.log(res.res);
                $('#field-append').html(res.res);
            }
        </script>
    </body>
</html>
