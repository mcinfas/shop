<?php

//include Product controller
include 'class/db.php';
include 'config/db_config.php';
include 'controller/ProductController.php';



if (isset($_POST['sku'])) {
    $sku = $_POST['sku'];
    $product = new ProductController();
    $is_exist = $product->checkSkuAlreadyExist($sku);
    header('Content-Type: json/application');
    echo json_encode(array('is_exist' => $is_exist));
}

