<?php

//Author : Infas
//Created on : 14/03/2020
//This controller made for products related actions
//include 'class/db.php';
//include 'config/db_config.php';

class ProductController {

    private $con;

    function __construct() {
        $this->con = new db(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function getProductsTypeFieldsId($id) {
        $product_fields = $this->con->query('SELECT id from product_type_fields WHERE product_type_id = ' . $id)->selectAll();
        return $product_fields;
    }

    public function checkSkuAlreadyExist($sku) {
        $product_fields = $this->con->query('SELECT id from products WHERE sku = ' . $sku)->selectAll();
        if (!empty($product_fields)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getProductsTypeFields($id) {
        $product_fields = $this->con->query('SELECT product_type_fields.*,fields.name as field_name,fields.field_type,fields.unit_type FROM product_type_fields inner join fields on fields.id = product_type_fields.field_id WHERE product_type_id = ' . $id)->selectAll();
        $html_input_fields = $this->convertToHtmlField($product_fields);
        return $html_input_fields;
    }

    public function getProductsType() {
        $produc_types = $this->con->query('SELECT * FROM product_type')->selectAll();

        return $produc_types;
    }

    public function getAllProducts() {
        $products = $this->con->query('SELECT * FROM products')->selectAll();
        $products_detail = $this->getProductsExtraFields($products);
        return $products_detail;
    }

    public function getProductsExtraFields($product_array) {
        for ($x = 0; $x < count($product_array); $x++) {

            $sql = 'SELECT 
    pfv.*, f.name AS field_name, f.unit_type
FROM
    product_filed_values pfv
        INNER JOIN
    fields f ON pfv.field_id = f.id
WHERE
    product_id = ' . $product_array[$x]['id'];
            $product_array[$x]['extra_fields'] = $this->con->query($sql)->selectAll();
        }
        return $product_array;
    }

    public function convertToHtmlField($fields_array) {
        $html_input = '';
        foreach ($fields_array as $field) {
            if ($field['field_type'] == 'number') {
                $html_input .= '<div class="form-group">
                    <label for="' . strtolower($field['field_name']) . '">' . $field['field_name'] . ' (' . $field['unit_type'] . ')</label>
                    <input type="number" class="form-control" id="' . strtolower($field['field_name']) . '" name="' . strtolower($field['id']) . '" placeholder="Enter ' . $field['field_name'] . '" required />
                </div>';
            }
        }
        return $html_input;
    }

    public function createFieldValueArray($product_id, $product_type_fields, $form_data) {
        foreach ($product_type_fields as $product_type_field) {
            foreach ($form_data as $key => $value) {
                if ($product_type_field['id'] == $key) {
                    $value_set[] = [
                        'field_id' => $key,
                        'value' => $value,
                        'product_id' => $product_id,
                    ];
                }
            }
        }
        return $value_set;
    }

    public function insertProductFieldsValue($insert_data_array) {
        foreach ($insert_data_array as $insert_data) {

            $sql = "INSERT INTO product_filed_values (product_id, field_id, field_value)
VALUES (" . $insert_data['product_id'] . "," . $insert_data['field_id'] . ",'" . $insert_data['value'] . "')";

            $res = $this->con->query($sql);
            if (!$res) {
                break;
            }
        }
        return $res;
    }

    public function insertProduct($data) {
        $sql = "INSERT INTO products (sku, name, price)
VALUES ('" . $data['sku'] . "','" . $data['name'] . "','" . $data['price'] . "')";
        $this->con->query($sql);
        return $this->con->lastInsertID();
    }

}
