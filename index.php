<?php
include 'class/db.php';
include 'config/db_config.php';
include 'controller/ProductController.php';
//connect database
$db = new db(DB_HOST, DB_USER, DB_PASS, DB_NAME);
// get product types
$product = new ProductController();
$products = $product->getAllProducts();
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Products</title>
        <style>
            .product-div {
                border: 1px solid;
                padding: 10px 20px;
                margin: 10px 10px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1 class="display-4 text-center">All Products</h1>
                <a class="btn btn-success float-right" href="add.php">Add New Product</a>
            </div>
            
            
            <div class="row">
                <?php
                if (!empty($products)) {
                    foreach ($products as $product) {
                        ?>
                        <div class="col-sm-4">
                            <div class="product-div">
                                <p><input type="checkbox" class="form-control pull-left"></p>
                                <p>SKU : <?= $product['sku']; ?></p>
                                <p>Name : <?= $product['name']; ?></p>
                                <p>Price : <?= $product['price']; ?></p>
                                <?php
                                if (!empty($product['extra_fields'])) {
                                    foreach ($product['extra_fields'] as $product) {
                                        foreach ($product as $key => $value) {
                                            if ($key == 'field_name') {
                                                ?>
                                                <p><?= $product['field_name']; ?> : <?= $product['field_value']; ?><?= $product['unit_type']?></p>
                                                <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <h5 class="text-center ">No Products Found.</h5>
                    <?php
                }
                ?>


            </div>

        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script>
            
        </script>
    </body>
</html>
