<?php

//include Product controller
include 'class/db.php';
include 'config/db_config.php';
include 'controller/ProductController.php';



if (isset($_POST['product_type_id'])) {
    $product_type_id = $_POST['product_type_id'];
    $product = new ProductController();
    $product_type_fields = $product->getProductsTypeFields($product_type_id);
    header('Content-Type: json/application');
    echo json_encode(array('res' => $product_type_fields));
}

