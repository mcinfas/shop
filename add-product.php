<?php

//include Product controller
include 'class/db.php';
include 'config/db_config.php';
include 'controller/ProductController.php';



if (isset($_POST['add-product'])) {
    $post_data = $_POST;
    $data = [
        'sku' => strip_tags($post_data['sku']),
        'name' => strip_tags($post_data['name']),
        'price' => strip_tags($post_data['price']),
        'type' => strip_tags($post_data['type']),
    ];




    //create project object
    $product = new ProductController();
    //insert product & get last insert ID
    $insert_product_id = $product->insertProduct($data);

    if (isset($insert_product_id)) {
        //get all product fields id by product type
        $product_type_fields = $product->getProductsTypeFieldsId($data['type']);
        //create field value array
        $field_value_array = $product->createFieldValueArray($insert_product_id, $product_type_fields, $post_data);

        //insert field values into database
        $insert_field_values = $product->insertProductFieldsValue($field_value_array);
        if ($insert_field_values) {
            //product addedd Success
            echo '<script>alert("Successfully Added.");window.location.href="index.php";</script>';
        } else {
            echo '<script>alert("Added Failed.");history.back();</script>';
        }
    } else {
        //product not added
        echo '<script>alert("Added Failed.");history.back();</script>';
    }
}

